package dao;

import conf.DbConfig;
import order.OrderJpa;
import order.OrderRow;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;

public class Tester {

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
                new AnnotationConfigApplicationContext(DbConfig.class);

        OrderDao dao = ctx.getBean(OrderDao.class);
        OrderRow row = new OrderRow("name",3,1);
        ArrayList<OrderRow> rows = new ArrayList<>();
        rows.add(row);

        dao.addOrder(new OrderJpa("nr", rows));
        dao.addOrder(new OrderJpa("aa"));

        System.out.println(dao.getAllOrders());
        //System.out.println(dao.getAllOrders());
        //System.out.println(dao.findOrderById(1L));
        System.out.println("single: " + dao.findOrderById(1L));
        dao.deleteOrder(1L);
        System.out.println(dao.getAllOrders());

        ctx.close();
    }
}
