package order;


import lombok.Data;

@Data
public class OrderReport {
    private int count;
    private int averageOrderAmount;
    private int turnoverWithoutVAT;
    private int turnoverVAT;
    private int turnoverWithVAT;
}
